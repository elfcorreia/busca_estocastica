#include "misc.hpp"
#include <random>

using namespace core;

void core::fill_rand(Populacao* populacao) {
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution;

	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {
		Individuo* individuo = populacao->getIndividuo(i);
		for (int j = 0, m = populacao->getTamanhoIndividuo(); j < m; j++) {
			float v = distribution(generator);
			individuo->setAtributo(j, v);
			//std::cout << v << " <--> " << individuo->getAtributo(j) << std::endl;
		}
	}
} 
