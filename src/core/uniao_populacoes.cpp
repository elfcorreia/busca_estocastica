#include "uniao_populacoes.hpp"
#include <sstream>
#include <exception>

using namespace core;

UniaoPopulacoes::UniaoPopulacoes(Populacao *populacao1, Populacao *populacao2) 
	: Populacao(populacao1->getTamanhoPopulacao() + populacao2->getTamanhoPopulacao(), populacao1->getTamanhoIndividuo()) {
	if (populacao1->getTamanhoIndividuo() != populacao2->getTamanhoIndividuo()) {
		throw new std::exception(); //"O tamanho dos individuos das duas populacoes deve ser o mesmo!"
	}
	//_tamanho_individuo = populacao1->getTamanhoIndividuo();
	//_tamanho_populacao = populacao1->getTamanhoPopulacao() + populacao2->getTamanhoPopulacao();
	_populacao1 = populacao1;
	_populacao2 = populacao2;	
}

float* UniaoPopulacoes::getDados() {
	throw new std::exception(); //"Não disponível"
}

Individuo* UniaoPopulacoes::getIndividuo(int indice) {
	if (indice < _populacao1->getTamanhoPopulacao()) {
		return _populacao1->getIndividuo(indice);
	} else {		
		return _populacao2->getIndividuo(indice - _populacao1->getTamanhoPopulacao());
	}
}

float UniaoPopulacoes::getAptidao(int indice) {
	if (indice < _populacao1->getTamanhoPopulacao()) {
		return _populacao1->getAptidao(indice);
	} else {		
		return _populacao2->getAptidao(indice - _populacao1->getTamanhoPopulacao());
	}
}

void UniaoPopulacoes::setAptidao(int indice, float valor) {
	throw new std::exception(); //"Não disponível"
}

std::string UniaoPopulacoes::toString() const {
	std::stringstream ss;
	ss << "<UniaoPopulacoes " << getTamanhoPopulacao() << "x" << getTamanhoIndividuo() << ">";
	return ss.str();
}