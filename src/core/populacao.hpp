#ifndef POPULACAO_HPP
#define POPULACAO_HPP

#include <string>

namespace core {
		
	class Populacao;	// Resolve dependencia circular
	
	class Individuo {
	public:
		virtual Populacao* 	getPopulacao() 														= 0;
		virtual int 		getTamanho() const													= 0;
		virtual float*		getDados() 															= 0;
		virtual float		getAtributo(int indice)												= 0;
		virtual void		setAtributo(int indice, float valor)								= 0;
		virtual float 		getAptidao() const													= 0;
		virtual void 		setAptidao(float valor) 											= 0;
		virtual void 		copiaAtributos(Individuo *outro, int inicio=0, int quantidade=-1) 	= 0;		
		virtual void 		clona(Individuo *outro)											= 0;		
		virtual std::string	toString() const													= 0;
	};

	class Populacao {	// classe abstrata base para PopulacaoCPU e PopulacaoGPU
	private:
		int _tamanho_populacao;
		int _tamanho_individuo;
		int _torneio(int tamanho = 2);
	public:				
		Populacao(int tamanho_populacao, int tamanho_individuo);
		virtual ~Populacao() { }
		
		Individuo*	pick();

		inline int 		getTamanhoPopulacao() const { return _tamanho_populacao; };
		inline int 		getTamanhoIndividuo() const { return _tamanho_individuo; };

		virtual float* 		getDados() = 0;
		virtual Individuo*	getIndividuo(int indice) = 0;
		virtual float 		getAptidao(int indice) = 0;
		virtual void 		setAptidao(int indice, float valor) = 0;
		virtual void		clona(Populacao *outra) = 0;
		virtual std::string toString() const = 0;
	};

}

#endif