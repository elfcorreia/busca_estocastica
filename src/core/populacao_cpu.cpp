#include "populacao_cpu.hpp"
#include <limits>
#include <random>
#include <sstream>
#include <iostream>
#include <cstring>

using namespace core;

PopulacaoCPU::PopulacaoCPU(int tamanho_populacao, int tamanho_individuo)
	: Populacao(tamanho_populacao, tamanho_individuo) 
{
	_dados = new float[tamanho_populacao * tamanho_individuo];
	_aptidoes = new float[tamanho_populacao];
}

PopulacaoCPU::~PopulacaoCPU() {
	delete[] _dados;
}

Individuo* PopulacaoCPU::getIndividuo(int indice) { 
	return new IndividuoCPU(this, indice);
}

void PopulacaoCPU::clona(Populacao* outra) {
	PopulacaoCPU* outraCPU = dynamic_cast<PopulacaoCPU*>(outra);

	bool tamanho_populacoes_diferentes = getTamanhoPopulacao() != outraCPU->getTamanhoPopulacao();
	bool tamanho_individuos_diferentes = getTamanhoIndividuo() != outraCPU->getTamanhoIndividuo();
	if (tamanho_populacoes_diferentes || tamanho_individuos_diferentes) {
		throw new std::exception(); //"O tamanho dos individuos e das populações deve ser o mesmo!"
	}
	std::memcpy(_dados, outraCPU->getDados(), getTamanhoPopulacao() * sizeof(float));
	std::memcpy(_aptidoes, outraCPU->_aptidoes, getTamanhoPopulacao() * getTamanhoIndividuo() * sizeof(float));
}

std::string PopulacaoCPU::toString() const {
	std::stringstream ss;
	ss << "<PopulacaoCPU " << getTamanhoPopulacao() << "x" << getTamanhoIndividuo() << ">";
	return ss.str();
}

IndividuoCPU::IndividuoCPU(PopulacaoCPU *origem, int indice) {
	_origem = origem;
	_indice = indice;
}

IndividuoCPU::~IndividuoCPU() { }

int IndividuoCPU::getTamanho() const { 
	return _origem->getTamanhoIndividuo(); 
}

float* IndividuoCPU::getDados() { 
	return _origem->getDados() + _indice * getTamanho(); 
}

float IndividuoCPU::getAtributo(int indice) { 
	return *(getDados() + indice);
}

void IndividuoCPU::setAtributo(int indice, float valor) { 
	*(getDados() + indice) = valor;
}

float IndividuoCPU::getAptidao() const { 
	return _origem->getAptidao(_indice); 
}

void IndividuoCPU::setAptidao(float valor) { 
	_origem->setAptidao(_indice, valor); 
}

// pré-condicoes
// 	inicio > 0
// 	quantidade < tamanho - inicio
void IndividuoCPU::copiaAtributos(Individuo *outro, int inicio, int quantidade) {
	quantidade = quantidade == -1 ? outro->getTamanho() - inicio : quantidade;

	float *dest = getDados() + inicio;
	float *src = outro->getDados() + inicio;
	//std::cout << "dest " << dest << " src " << src << " " << inicio << " " << quantidade << std::endl;
	std::memcpy(dest, src, quantidade * sizeof(float));
}

void IndividuoCPU::clona(Individuo *outro) {
	std::memcpy(getDados(), outro->getDados(), getTamanho() * sizeof(float));
	setAptidao(outro->getAptidao());
}

std::string IndividuoCPU::toString() const {
	std::stringstream ss;
	ss << "<IndividuoCPU tamanho=" << getTamanho() << " aptidao=" << getAptidao() << ">";
	return ss.str();
}