#ifndef POPULACAO_CPU_HPP
#define POPULACAO_CPU_HPP

#include "populacao.hpp"

namespace core {
		
	class PopulacaoCPU: public Populacao {
	private:
		float *_dados;
		float *_aptidoes;
	public:
		PopulacaoCPU(int tamanho_populacao, int tamanho_individuo);
		~PopulacaoCPU();

		inline float* getDados() 							{ return _dados; 				};
		inline float getAptidao(int indice) 		 		{ return _aptidoes[indice];		};
		inline void setAptidao(int indice, float valor) 	{ _aptidoes[indice] = valor;	};
		
		Individuo* getIndividuo(int indice);		
		void clona(Populacao *outra);
		std::string toString() const;
	};

	// Vazando objetos Individuos* o tempo todo. REVER
	class IndividuoCPU: public Individuo {
			private:
				PopulacaoCPU *_origem;
				int _indice;
			public:
				IndividuoCPU(PopulacaoCPU *origem, int indice);
				~IndividuoCPU();
				
				inline Populacao* 	getPopulacao() { return _origem; };

				int 		getTamanho() const;
				float* 		getDados();
				float		getAtributo(int indice);
				void		setAtributo(int indice, float valor);
				float 		getAptidao() const;
				void 		setAptidao(float valor);
				void 		copiaAtributos(Individuo *outro, int inicio=0, int quantidade=-1);				
				void 		clona(Individuo *outro);
				std::string toString() const;
		};

}

#endif