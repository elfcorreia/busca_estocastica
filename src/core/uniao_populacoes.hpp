#ifndef UNIAO_POPULACOES_HPP
#define UNIAO_POPULACOES_HPP

#include "populacao.hpp"

namespace core {
		
	class UniaoPopulacoes: public Populacao {
	private:
		Populacao *_populacao1;
		Populacao *_populacao2;
	public:
		UniaoPopulacoes(Populacao *populacao1, Populacao *populacao2);		
		~UniaoPopulacoes() { };

		float* 	getDados();
		Individuo*	getIndividuo(int indice);
		float 		getAptidao(int indice);
		void 		setAptidao(int indice, float valor);
		std::string toString() const;

	};

}

#endif