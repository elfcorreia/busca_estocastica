#include "populacao.hpp"
#include <limits>
#include <random>

using namespace core;

Populacao::Populacao(int tamanho_populacao, int tamanho_individuo) {
	_tamanho_populacao = tamanho_populacao;
	_tamanho_individuo = tamanho_individuo;
}

int Populacao::_torneio(int tamanho) {
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, _tamanho_populacao);
  
	float champion_fit = std::numeric_limits<float>::max();
	int champion_idx = -1;
	for (int i = 0; i < tamanho; i++) {
		int idx = distribution(generator);
		int idx_apt = getAptidao(idx);
		if (idx_apt < champion_fit) {
			champion_fit = idx_apt;
			champion_idx = idx;
		}
	}
	return champion_idx;
}

Individuo *Populacao::pick() {	
	return getIndividuo(_torneio());
}