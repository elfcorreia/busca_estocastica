#ifndef POPULACAO_MISC_HPP
#define POPULACAO_MISC_HPP

#include "populacao.hpp"

namespace core {
		
	void fill_rand(core::Populacao* populacao);

}

#endif