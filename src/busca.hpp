#ifndef BUSCA_HPP
#define BUSCA_HPP

//#include "busca/estrategia_busca.hpp"
#include "busca/aptidoes/avaliador_aptidao.hpp"
#include "busca/aptidoes/himmelblau.hpp"
#include "busca/aptidoes/interpretador_aptidao.hpp"
#include "busca/aptidoes/mean_square_difference.hpp"
#include "busca/aptidoes/rastrigin.hpp"
#include "busca/aptidoes/sphere.hpp"

#include "busca/estrategias/comum/combinacao_op.hpp"
#include "busca/estrategias/comum/cross_comb_op.hpp"

#endif