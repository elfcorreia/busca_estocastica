#include "populacao_parallel_coordinates_view.hpp"
#include <sstream>

using namespace visualizacao;

std::string PopulacaoParallelCoordinatesView::plot(core::Populacao *populacao) {
 	std::stringstream out;
 	out << "# Gerado por BuscaEstocastica" << std::endl;
 	out << "# Use com gnuplot >= 5.2" << std::endl;
 	out << std::endl;

 	out << "set title \"Parallel Axis Plot\" font \",15\"" << std::endl; 	
 	out << std::endl;
	
	out << "set border 0" << std::endl;
	out << "unset ytics" << std::endl;	
	out << "unset key" << std::endl;
	out << std::endl;
	
	out << "set xtics 1 format \"Atributo %g\" scale 0,0" << std::endl;	
	out << "set xrange [] noextend" << std::endl;
	out << "set paxis 1 tics" << std::endl;
	out << "set paxis " << populacao->getTamanhoIndividuo() << " tics left offset 4" << std::endl;
	out << std::endl;

	out << "$dados << EOD" << std::endl;
	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {		
		core::Individuo* individuo = populacao->getIndividuo(i);
		for (int j = 0, m = populacao->getTamanhoIndividuo(); j < m; j++) {
			out << individuo->getAtributo(j) << " ";
		}
		out << individuo->getAptidao() << std::endl;
	}
	out << "EOD" << std::endl;
	out << std::endl;

	out << "plot \"$dados\" using ";
	if (populacao->getTamanhoIndividuo() > 0) {
		out << 1;
	}
	for (int i = 1; i < populacao->getTamanhoIndividuo(); i++) {
		out << ":" << i + 1;
	}
	out << " with parallel lt 1 lw 2" << std::endl;

	return out.str();
}