#ifndef POPULACAO_PARALLEL_COORDINATES_VIEW_HPP
#define POPULACAO_PARALLEL_COORDINATES_VIEW_HPP

#include "populacao_view.hpp"

namespace visualizacao {

	class PopulacaoParallelCoordinatesView: public PopulacaoView {
	public:
		std::string plot(core::Populacao* populacao);
	};

}

#endif