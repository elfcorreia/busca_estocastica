#ifndef POPULACAO_VIEW_HPP
#define POPULACAO_VIEW_HPP

#include "../core/populacao.hpp"
#include <string>

namespace visualizacao {

	class PopulacaoView {
	public:
		virtual std::string plot(core::Populacao* populacao) = 0;
	};

}

#endif