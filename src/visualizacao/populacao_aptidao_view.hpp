#ifndef POPULACAO_APTIDAO_VIEW_HPP
#define POPULACAO_APTIDAO_VIEW_HPP

#include "populacao_view.hpp"

namespace visualizacao {

	class PopulacaoAptidaoView: public PopulacaoView {
	public:
		std::string plot(core::Populacao* populacao);
	};

}

#endif