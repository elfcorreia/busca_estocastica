#include "populacao_aptidao_view.hpp"
#include <sstream>

using namespace visualizacao;

std::string PopulacaoAptidaoView::plot(core::Populacao *populacao) {
	std::stringstream out;
	
	out << "# Gerado por BuscaEstocastica" << std::endl;
 	out << "# Use com gnuplot >= 5.2" << std::endl;
 	out << std::endl;

 	out << "set title \"Parallel Axis Plot\" font \",15\"" << std::endl; 	
 	out << std::endl;
	
	out << "set title \"Aptidão da População\"" << std::endl;
	out << "set style data linespoints" << std::endl;
	out << "set style fill solid border -1" << std::endl;
	out << "set boxwidth 0.1" << std::endl;


	out << "binwidth=5" << std::endl;
	out << "bin(x, width)=width*floor(x/width)" << std::endl;
	out << "$dados << EOD" << std::endl;
	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {
		out << populacao->getAptidao(i) << std::endl;
	}
	out << "EOD" << std::endl;

	out << "plot \"$dados\" using 1 title \"aptidao\" smooth freq with boxes" << std::endl;

	return out.str();
}