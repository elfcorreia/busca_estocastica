#ifndef CORE_HPP
#define CORE_HPP

#include "core/populacao.hpp"
#include "core/misc.hpp"
#include "core/populacao_cpu.hpp"
#include "core/populacao_gpu.hpp"
#include "core/uniao_populacoes.hpp"

#endif