#ifndef PERSISTENCIA_HPP
#define PERSISTENCIA_HPP

#include "persistencia/populacao_serializer.hpp"
#include "persistencia/populacao_tabular_serializer.hpp"
#include "persistencia/populacao_json_serializer.hpp"

#endif