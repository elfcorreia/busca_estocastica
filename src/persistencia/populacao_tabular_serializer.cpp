#include "populacao_tabular_serializer.hpp"
#include "../core/populacao_cpu.hpp"

using namespace persistencia;

core::Populacao* PopulacaoTabularSerializer::load(std::istream& input) {
	int tamanho_populacao;
	int tamanho_individuo;
	input >> tamanho_populacao;
	input >> tamanho_individuo;
	core::Populacao* populacao = new core::PopulacaoCPU(tamanho_populacao, tamanho_individuo);
	for (int i = 0; i < tamanho_populacao; i++) {
		core::Individuo* individuo = populacao->getIndividuo(i);
		float aptidao;
		input >> aptidao;
		individuo->setAptidao(aptidao);
		for (int j = 0; j < tamanho_individuo; j++) {
			float valor;
			input >> valor;
			individuo->setAtributo(j, valor);
		}
	}
	return populacao;
}

void PopulacaoTabularSerializer::save(core::Populacao* populacao, std::ostream &output) {
	int tamanho_individuo = populacao->getTamanhoIndividuo();
	output << populacao->getTamanhoPopulacao() << " " << tamanho_individuo << std::endl;
	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {
		core::Individuo* individuo = populacao->getIndividuo(i);
		output << " " << individuo->getAptidao();
		for (int j = 0; j < tamanho_individuo; j++) {
			output << " " << individuo->getAtributo(j);
		}
		output << std::endl;
	}
}