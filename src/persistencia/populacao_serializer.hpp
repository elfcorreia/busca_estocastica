#ifndef PERSISTENCIA_SERIALIZER_HPP
#define PERSISTENCIA_SERIALIZER_HPP

#include "../core/populacao.hpp"
#include <iostream>

namespace persistencia {

	class PopulacaoSerializer {
	public:
		virtual core::Populacao* load(std::istream& input) = 0;
		virtual void save(core::Populacao* populacao, std::ostream& output) = 0;
	};

}

#endif