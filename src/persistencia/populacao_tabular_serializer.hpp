#ifndef POPULACAO_TABULAR_SERIALIZER_HPP
#define POPULACAO_TABULAR_SERIALIZER_HPP

#include "populacao_serializer.hpp"

namespace persistencia {

	class PopulacaoTabularSerializer: public PopulacaoSerializer {
	public:
		core::Populacao* load(std::istream& input);
		void save(core::Populacao* populacao, std::ostream& output);
	};

}

#endif