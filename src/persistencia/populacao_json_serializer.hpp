#ifndef POPULACAO_JSON_SERIALIZER_HPP
#define POPULACAO_JSON_SERIALIZER_HPP

#include "populacao_serializer.hpp"

namespace persistencia {

	class PopulacaoJSONSerializer: public PopulacaoSerializer {
	public:
		core::Populacao* load(std::istream& input);
		void save(core::Populacao* populacao, std::ostream& output);
	};

}

#endif