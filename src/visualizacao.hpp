#ifndef VISUALIZACAO_HPP
#define VISUALIZACAO_HPP

#include "visualizacao/populacao_view.hpp"
#include "visualizacao/populacao_parallel_coordinates_view.hpp"
#include "visualizacao/populacao_aptidao_view.hpp"

#endif