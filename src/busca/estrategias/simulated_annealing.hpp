#ifndef SIMULATED_ANNEALING_HPP
#define SIMULATED_ANNEALING_HPP

#include "../../core/populacao.hpp"
#include "../aptidoes/avaliador_aptidao.hpp"
#include "../callbacks/callback.hpp"

namespace busca::estrategias {

	class SimulatedAnnealing: public EstrategiaBusca {
	public:
		core::Populacao* evolui(core::Populacao* populacao, AvaliadorAptidao* aptidoes::avaliador);
		void addCallback(callbacks::Callback* callback);
		void removeCallback(callbacks::Callback* callback);
	};

}

#endif