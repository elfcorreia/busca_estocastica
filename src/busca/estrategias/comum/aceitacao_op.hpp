#ifndef ACEITACAO_OP_HPP
#define ACEITACAO_OP_HPP

#include "../../../core/populacao.hpp"

namespace busca::estrategias::comum {

	class AceitacaoOp {
	public:
		virtual bool aceita(real_t temperatura, core::Individuo* individuo, core::Individuo* novo_individuo) = 0;
	};

}

#endif