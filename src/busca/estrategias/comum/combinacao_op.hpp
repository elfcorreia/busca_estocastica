#ifndef COMBINACAO_OP_HPP
#define COMBINACAO_OP_HPP

#include "../../../core/populacao.hpp"
#include <vector>

namespace busca::estrategias::comum {
	
	class CombinacaoOp {
	private:
		int _qtdPais;
		int _qtdFilhos;
	public:
		CombinacaoOp(int qtdPais, int qtdFilhos) { _qtdPais = qtdPais; _qtdFilhos = qtdFilhos; };
		virtual ~CombinacaoOp() { }

		inline int getQtdPais() { return _qtdPais; };
		inline int getQtdFilhos() { return _qtdFilhos; };
		virtual void combina(int tamanho_individuo, std::vector<core::Individuo*> pais, std::vector<core::Individuo*> filhos) = 0;
	};

}

#endif