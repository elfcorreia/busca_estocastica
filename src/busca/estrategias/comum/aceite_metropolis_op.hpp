#ifndef ACEITE_METROPOLIS_HPP
#define ACEITE_METROPOLIS_HPP

#include "aceitacao_op.hpp"

namespace busca::estrategias::comum {

	class AceiteMetropolisOp: public AceitacaoOp {
	public:
		bool aceita(real_t temperatura, core::Individuo* individuo, core::Individuo* novo_individuo);
	};
}

#endif