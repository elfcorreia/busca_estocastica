#ifndef MUTACAO_ALEATORIA_HPP
#define MUTACAO_ALEATORIA_HPP

#include "mutacao_op.hpp"

namespace busca::estrategias::comum {

	class MutacaoAleatoriaOp: public MutacaoOp {
	public:
		void muta(core::Individuo* individuo);
	};

}

#endif