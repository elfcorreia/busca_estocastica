#include "cross_comb_op.hpp"
#include <iostream>

using namespace busca::estrategias::comum;

void CrossCombOp::combina(int tamanho_individuo, std::vector<core::Individuo*> pais, std::vector<core::Individuo*> filhos, int ponto_corte) {
	filhos[0]->copiaAtributos(pais[0], 0, ponto_corte);
	filhos[0]->copiaAtributos(pais[1], ponto_corte, -1);

	filhos[1]->copiaAtributos(pais[1], 0, ponto_corte);
	filhos[1]->copiaAtributos(pais[0], ponto_corte, -1);
}

void CrossCombOp::combina(int tamanho_individuo, std::vector<core::Individuo*> pais, std::vector<core::Individuo*> filhos) {
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, tamanho_individuo - 1);
	combina(tamanho_individuo, pais, filhos, distribution(generator));
}