#ifndef CROSS_COMB_OP_HPP
#define CROSS_COMB_OP_HPP

#include "combinacao_op.hpp"
#include <random>

namespace busca::estrategias::comum {

	class CrossCombOp: public CombinacaoOp {		
	public:
		CrossCombOp() : CombinacaoOp(2, 2) { }
		~CrossCombOp() { }
		
		// ponto corte - 0 based index
		void combina(int tamanho_individuo, std::vector<core::Individuo*> pais, std::vector<core::Individuo*> filhos);

		// bom para testar mas adiciona mais uma chamada de procedimento na pilha de execução
		void combina(int tamanho_individuo, std::vector<core::Individuo*> pais, std::vector<core::Individuo*> filhos, int ponto_corte);
	};

}

#endif