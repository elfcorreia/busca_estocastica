#ifndef MUTACAO_OP_HPP
#define MUTACAO_OP_HPP

#include "../../../core/populacao.hpp"

namespace busca::estrategias::comum {

	class MutacaoOp {
	public:
		virtual void muta(core::Individuo* individuo) = 0;
	};

}

#endif