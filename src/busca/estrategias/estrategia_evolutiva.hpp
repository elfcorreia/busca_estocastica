#ifndef ESTRATEGIA_EVOLUTIVA_HPP
#define ESTRATEGIA_EVOLUTIVA_HPP

#include "../estrategia_busca.hpp"
#include "comum/combinacao_op.hpp"
#include "comum/mutacao_op.hpp"
#include <vector>

namespace busca::estrategias {

	//TODO: Colocar os métodos para gerenciar os callbacks na clase EstrategiaBusca
	class EstrategiaEvolutiva: public busca::EstrategiaBusca {
	private:
		std::vector<busca::callbacks::Callback*> _callbacks;
		float _offspring_factor;
		float _combination_probability;
		float _mutation_probability;
		comum::CombinacaoOp* _combinacao_op;
		comum::MutacaoOp* _mutacao_op;
	public:
		EstrategiaEvolutiva() { }
		~EstrategiaEvolutiva() { }

		inline comum::CombinacaoOp* getCombinacaoOp()		{ return _combinacao_op;			}
		inline comum::MutacaoOp* getMutacaoOp()				{ return _mutacao_op;				}
		inline float getOffspringFactor() 					{ return _offspring_factor; 		}
		inline float getCombinationProbability() 			{ return _combination_probability; 	}
		inline float getMutationProbability() 				{ return _mutation_probability; 	}

		inline void getCombinacaoOp(comum::CombinacaoOp* v)	{ _combinacao_op = v;				}
		inline void getMutacaoOp(comum::MutacaoOp* v)		{ _mutacao_op = v;					}
		inline void setOffspringFactor(float v) 			{ _offspring_factor = v; 			}
		inline void setCombinationProbability(float v) 		{ _combination_probability = v; 	}
		inline void setMutationProbability(float v) 		{ _mutation_probability = v; 		}

		void addCallback(callbacks::Callback* callback);
		void removeCallback(callbacks::Callback* callback);

		core::Populacao* evolui(core::Populacao* populacao, aptidoes::AvaliadorAptidao* avaliador);
	};	

}

#endif