#ifndef ESTRATEGIA_BUSCA_HPP
#define ESTRATEGIA_BUSCA_HPP

#include "../core/populacao.hpp"
#include "aptidoes/avaliador_aptidao.hpp"
#include "callbacks/callback.hpp"

namespace busca {

	class EstrategiaBusca {
	public:
		virtual core::Populacao* evolui(core::Populacao* populacao, aptidoes::AvaliadorAptidao* avaliador) = 0;
		virtual void addCallback(callbacks::Callback* callback) = 0;
		virtual void removeCallback(callbacks::Callback* callback) = 0;
	};

}

#endif