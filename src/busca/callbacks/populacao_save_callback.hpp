#ifndef POPULACAO_SAVE_CALLBACK_HPP
#define POPULACAO_SAVE_CALLBACK_HPP

#include "callback.hpp"

namespace busca::callbacks {

	class PopulacaoSaveCallback: public Callback {
		public:
			void ao_inicio_evolucao(int geracao, core::Populacao *populacao);
			void ao_fim_geracao(int geracao, core::Populacao *populacao);
			void ao_fim_evolucao(int geracao, core::Populacao *populacao);
	};

}

#endif