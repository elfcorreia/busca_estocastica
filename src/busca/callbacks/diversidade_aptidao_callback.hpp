#ifndef DIVERSIDADE_APTIDAO_CALLBACK_HPP
#define DIVERSIDADE_APTIDAO_CALLBACK_HPP

#include "callback.hpp"

namespace busca::callbacks {
	
	class DiversidadeAptidaoCallback: public Callback {
		public:
			void ao_inicio_evolucao(int geracao, core::Populacao *populacao);
			void ao_fim_geracao(int geracao, core::Populacao *populacao);
			void ao_fim_evolucao(int geracao, core::Populacao *populacao);
	};

}

#endif