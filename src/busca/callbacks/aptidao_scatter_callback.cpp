#include "aptidao_scatter_callback.hpp"
#include <iostream>

using namespace busca::callbacks;

void AptidaoScatterCallback::ao_inicio_evolucao(int geracao, core::Populacao *populacao) {

}

void AptidaoScatterCallback::ao_fim_geracao(int geracao, core::Populacao *populacao) {
	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {
		std::cout << geracao << " " << populacao->getAptidao(i) << std::endl;
	}
}

void AptidaoScatterCallback::ao_fim_evolucao(int geracao, core::Populacao *populacao) {

}