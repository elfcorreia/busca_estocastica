#ifndef APTIDAO_SCATTER_CALLBACK_HPP
#define APTIDAO_SCATTER_CALLBACK_HPP

#include "callback.hpp"

namespace busca::callbacks {

	class AptidaoScatterCallback: public Callback {
		public:
			void ao_inicio_evolucao(int geracao, core::Populacao *populacao);
			void ao_fim_geracao(int geracao, core::Populacao *populacao);
			void ao_fim_evolucao(int geracao, core::Populacao *populacao);
	};

}

#endif