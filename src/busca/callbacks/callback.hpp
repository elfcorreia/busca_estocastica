#ifndef CALLBACK_HPP
#define CALLBACK_HPP

#include "../../core/populacao.hpp"

namespace busca::callbacks {

	class Callback {
	public:
		virtual void ao_inicio_evolucao(int geracao, core::Populacao *populacao) = 0;
		virtual void ao_fim_geracao(int geracao, core::Populacao *populacao) = 0;
		virtual void ao_fim_evolucao(int geracao, core::Populacao *populacao) = 0;
	};

}

#endif