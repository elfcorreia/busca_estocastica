#ifndef APTIDAO_MEDIA_HPP
#define APTIDAO_MEDIA_HPP

#include "callback.hpp"

namespace busca::callbacks {

	class AptidaoMediaCallback: public Callback {
		public:
			void ao_inicio_evolucao(int geracao, core::Populacao *populacao);
			void ao_fim_geracao(int geracao, core::Populacao *populacao);
			void ao_fim_evolucao(int geracao, core::Populacao *populacao);
	};

}

#endif