#ifndef INTERPRETADOR_APTIDAO_HPP
#define INTERPRETADOR_APTIDAO_HPP

#include "avaliador_aptidao.hpp"

namespace busca::aptidoes {

	class InterpretadorAptidao: public AvaliadorAptidao {
	public:
		void eval(core::Populacao* populacao);	
	};		

	class Operacoes {

	};

	class OperacoesCPU: public Operacoes {

	};
	class OperacoesGPU: public Operacoes {

	};
}

#endif