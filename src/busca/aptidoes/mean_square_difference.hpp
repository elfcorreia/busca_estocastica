#ifndef MEAN_SQUARE_DIFFERENCE_HPP
#define MEAN_SQUARE_DIFFERENCE_HPP

#include "avaliador_aptidao.hpp"

namespace busca::aptidoes {
	class MeanSquareDifference: public AvaliadorAptidao {
	public:
		void eval(core::Populacao* populacao);
	};
}

#endif