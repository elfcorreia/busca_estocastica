#ifndef RASTRIGIN_HPP
#define RASTRIGIN_HPP

#include "avaliador_aptidao.hpp"

namespace busca::aptidoes {
	class Rastrigin: public AvaliadorAptidao {
	public:
		void eval(core::Populacao* populacao);
	};
}

#endif