#ifndef AVALIADOR_APTIDAO_HPP
#define AVALIADOR_APTIDAO_HPP

#include "../../core/populacao.hpp"

namespace busca::aptidoes {

	class AvaliadorAptidao {
	public:
		AvaliadorAptidao() { }
		virtual ~AvaliadorAptidao() { }
		
		virtual void eval(core::Populacao* populacao) = 0;
	};

}

#endif