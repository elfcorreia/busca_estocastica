#include "sphere.hpp"

using namespace busca::aptidoes;

void Sphere::eval(core::Populacao* populacao) {

	// TODO: oportunidade para paralelização com omp
	for (int i = 0, n = populacao->getTamanhoPopulacao(); i < n; i++) {
		core::Individuo* individuo = populacao->getIndividuo(i);
		
		float sum = 0;
		for (int j = 0, m = populacao->getTamanhoIndividuo(); j < m; j++) {
			float v = individuo->getAtributo(j);
			sum += v*v;
		}

		individuo->setAptidao(sum);
	}
}