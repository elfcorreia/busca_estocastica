#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "avaliador_aptidao.hpp"

namespace busca::aptidoes {
	class Sphere: public AvaliadorAptidao {
	public:
		void eval(core::Populacao* populacao);
	};
}

#endif