#ifndef HIMMELBLAU_HPP
#define HIMMELBLAU_HPP

#include "avaliador_aptidao.hpp"

namespace busca::aptidoes {
	class Himmelblau: public AvaliadorAptidao {
	public:
		void eval(core::Populacao* populacao);
	};
}

#endif