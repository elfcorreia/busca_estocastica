#include "util.hpp"
#include <iostream>

void print_individuo(core::Individuo* individuo) {
	std::cout << "{";
	for (int i = 0, n = individuo->getTamanho(); i < n; i++) {
		std::cout << individuo->getAtributo(i) << " ";
	}
	std::cout << "}";
	//std::cout << std::endl;
}
void print_list_initializer(std::initializer_list<float> l) {
	std::cout << "{";
	for (auto a :l) {
		std::cout << a << " ";
	}
	std::cout << "}";
}
void print_populacao(core::Populacao* populacao) {
	for (int i = 0; i < populacao->getTamanhoPopulacao(); i++) {
		print_individuo(populacao->getIndividuo(i));
	}
}

void print_populacao(std::vector<core::Individuo*> &v) {
	for (auto i: v) {		
		print_individuo(i);
	}
}

void set_atributos_individuos(core::Individuo* individuo, std::initializer_list<float> atributos) {	
	int i = 0;
	for (auto atributo: atributos) {
		individuo->setAtributo(i, atributo);
		i += 1;
	}
}

bool assert_atributos_individuos(core::Individuo* individuo, std::initializer_list<float> atributos) {
	int i = 0;
	for (auto atributo: atributos) {
		float obs = individuo->getAtributo(i);
		if (obs != atributo) {
			std::cout << "ERROR: esperado "; 
			print_list_initializer(atributos);
			std::cout << ", mas veio ";
			print_individuo(individuo);
			std::cout << std::endl;
			return false;
		}
		i++;
	}
	return true;
}