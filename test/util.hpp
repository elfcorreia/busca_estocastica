#ifndef UTIL_HPP
#define UTIL_HPP

#include "core.hpp"
#include "busca.hpp"
#include <vector>

void print_individuo(core::Individuo* individuo);
void print_populacao(core::Populacao* populacao);
void print_populacao(std::vector<core::Individuo*> &v);
void set_atributos_individuos(core::Individuo* individuo, std::initializer_list<float> atributos);
bool assert_atributos_individuos(core::Individuo* individuo, std::initializer_list<float> atributos);

#endif