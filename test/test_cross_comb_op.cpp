#include "catch.hpp"
#include "util.hpp"

bool test_cross_comb_op(std::initializer_list<float> a, std::initializer_list<float> b, int ponto_corte, std::initializer_list<float> c, std::initializer_list<float> d) {
	int tamanho_populacao = 2;
	int tamanho_individuo = a.size();

	core::Populacao *p = new core::PopulacaoCPU(tamanho_populacao, tamanho_individuo);
	set_atributos_individuos(p->getIndividuo(0), a);
	set_atributos_individuos(p->getIndividuo(1), b);
	//print_populacao(p);
	
	std::vector<core::Individuo*> pais;
	pais.push_back(p->getIndividuo(0));
	pais.push_back(p->getIndividuo(1));
	
	core::Populacao *o = new core::PopulacaoCPU(tamanho_populacao, tamanho_individuo);
	
	std::vector<core::Individuo*> filhos;	
	filhos.push_back(o->getIndividuo(0));
	filhos.push_back(o->getIndividuo(1));

	busca::estrategias::comum::CrossCombOp* op = new busca::estrategias::comum::CrossCombOp();
	op->combina(tamanho_individuo, pais, filhos, ponto_corte);

	//std::cout << "--" << std::endl;
	//print_populacao(filhos);
	if (!assert_atributos_individuos(o->getIndividuo(0), c)) {
		return false;
	}
	return assert_atributos_individuos(o->getIndividuo(1), d);
}

TEST_CASE( "Individuals are cross-combined", "[cross-comb-op]" ) {
    REQUIRE( test_cross_comb_op({1, 1}, {0, 0}, 1, {1, 0}, {0, 1}) );
    REQUIRE( test_cross_comb_op({1, 1, 1}, {0, 0, 0}, 1, {1, 0, 0}, {0, 1, 1}) );
    REQUIRE( test_cross_comb_op({1}, {0}, 0, {0}, {1}) );
    REQUIRE( test_cross_comb_op({1, 1, 1, 1, 1}, {0, 0, 0, 0, 0}, 3, {1, 1, 1, 0, 0}, {0, 0, 0, 1, 1}) );
}