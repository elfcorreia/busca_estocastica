# Sistema de busca estocástica

Software desenvolvido ao longo da disciplina GA031 - Arquitetura de Software ministrada pelo professor Antonio Tadeu na primavera de 2018.

## Dependencias
 * getopt - disponíveis nas principais distribuições gnu/linux
 * cmake >= 3.10 - para compilação dos binários e testes
 * gnuplot >= 5 - para visualização dos gráficos

## Compilação

Para compilar:
```
 $ mkdir build
 $ cd build
 $ cmake ..
 $ make
```

Se tudo ocorrer bem, serão criados dois binários: `busca_cli`, o programa de busca estocástica em si, e `busca_test` um binário catch2 que ao ser executados executa uma bateria de testes.

## Execução dos testes
 * Após compilar os fontes, basta executar o binário `busca_test`

Emerson C. Lima
16/01/2019