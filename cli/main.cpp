#include <iostream>
#include <fstream>
#include "core.hpp"
#include "visualizacao.hpp"
#include "persistencia.hpp"
#include "busca.hpp"

void acao_criar(int tamanho_populacao, int tamanho_individuo) {
    core::PopulacaoCPU populacao(tamanho_populacao, tamanho_individuo);
    core::fill_rand(&populacao);
    persistencia::PopulacaoTabularSerializer serializer;
    serializer.save(&populacao, std::cout);
}

void acao_visualizar(char *nome_arquivo, char* tipo) {
    visualizacao::PopulacaoView* visualizador;
    
    // Precisamos de uma Abstract Factory para os visualizadores
    std::string stipo("parallel");
    if (tipo != nullptr) {
        stipo = tipo;
    }
    
    if (stipo == "parallel") {
        visualizador = new visualizacao::PopulacaoParallelCoordinatesView();
    } else if (stipo == "aptidao") {
        visualizador = new visualizacao::PopulacaoAptidaoView();
    } else {
        std::cerr << "O tipo de visualização " << stipo << " inválido" << std::endl;
    }

    persistencia::PopulacaoTabularSerializer serializer;
    std::ifstream arquivo(nome_arquivo);
    core::Populacao* populacao = serializer.load(arquivo);
    
    std::cout << visualizador->plot(populacao);

    delete populacao;
}

void acao_evoluir(char* nome_arquivo, char* aptidao) {
    //TODO: terminar
    persistencia::PopulacaoTabularSerializer serializer;
    std::ifstream arquivo(nome_arquivo);
    core::Populacao* populacao = serializer.load(arquivo);

    //busca::EstrategiaBusca *estrategia = new busca::estrategia::EstrategiaEvolutiva();
    busca::aptidoes::AvaliadorAptidao *a = new busca::aptidoes::Sphere();
    a->eval(populacao);
    serializer.save(populacao, std::cout);
    
    delete a;
    //delete estrategia;
}

void test() {
    core::Populacao* p = new core::PopulacaoCPU(5, 3);
    std::cout << p->toString() << std::endl << std::endl;

    auto serializer = persistencia::PopulacaoTabularSerializer();
    serializer.save(p, std::cout);
    std::cout << "apos fill_rand():" << std::endl;
    core::fill_rand(p);
    serializer.save(p, std::cout);
    std::cout << "---" << std::endl;
    
    visualizacao::PopulacaoParallelCoordinatesView view;
    std::cout << view.plot(p) << std::endl;
}

void print_usage(char *nome_arquivo) {
    std::cout << "Sistema de busca aleatória" << std::endl;
    std::cout << "Por Emerson Lima em 2019 - ga031-arquitetura de software" << std::endl;
    std::cout << std::endl; 
    std::cout << "Uso " << nome_arquivo << " [comando] [-h]" << std::endl;  
    std::cout << std::endl; 
    std::cout << "Comandos" << std::endl;   
    std::cout << std::endl; 
    std::cout << "\tcriar"      << "\t\tCria uma nova populacao"    << std::endl;   
    std::cout << "\tvisualizar" << "\tVisualiza uma populacao"  << std::endl;   
    std::cout << "\tevoluir"    << "\t\tEvolui uma opulacao"        << std::endl;
    std::cout << std::endl; 
}

/*************
 * main
 *************/
extern void parse_args(int argc, char* argv[]);

int main(int argc, char* argv[]) {
    parse_args(argc, argv);
    return 0;
}