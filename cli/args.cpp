#include <string>
#include <cstring>
#include <iostream>
#include <getopt.h>

extern void acao_visualizar(char* nome_arquivo, char* tipo);
extern void acao_criar(int tamanho_populacao, int tamanho_individuo);
extern void acao_evoluir(char* nome_arquivo, char* aptidao);

static void print_usage(char *programa) {
	std::cerr << "Sistema de busca aleatória" << std::endl;
	std::cerr << "Por Emerson Lima em 2019 - ga031-arquitetura de software" << std::endl;
	std::cerr << std::endl;	
	std::cerr << "Uso " << programa << " [comando] [-h]" << std::endl;	
	std::cerr << std::endl;	
	std::cerr << "Comandos" << std::endl;	
	std::cerr << std::endl;	
	std::cerr << "\tcriar" 		<< "\t\tCria uma nova populacao" 	<< std::endl;	
	std::cerr << "\tvisualizar" << "\tVisualiza uma populacao" 	<< std::endl;	
	std::cerr << "\tevoluir" 	<< "\t\tEvolui uma opulacao" 		<< std::endl;
	std::cerr << std::endl;	
}

static void print_criar_usage(char* programa) {
    std::cerr << "Comando Criar" << std::endl;
    std::cerr << "Uso: " << programa << " criar -p tamanho_populacao -i tamanho_individuo" << std::endl;
}

static void parse_criar_args(char* programa, int argc, char* argv[]) {
	if (argc == 1) {
        std::cerr << "Esperava opções" << std::endl;
		std::cerr << "use " << programa << " criar -h para ajuda" << std::endl;
		exit(0);
    }
    int c;
    int tamanho_populacao = -1;
    int tamanho_individuo = -1;
    while((c = getopt(argc, argv, "p:i:h")) != -1){
        switch(c) {
        case 'p':
        	tamanho_populacao = std::stoi(optarg);
        	break;
        case 'i':
        	tamanho_individuo = std::stoi(optarg);
        	break;
        case 'h':
            print_criar_usage(programa);
            exit(EXIT_SUCCESS);
        case '?':
			std::cerr << "Use " << programa << " criar -h para ajuda" << std::endl;
            exit(EXIT_FAILURE);
    	}
	}	
	if (tamanho_populacao == -1 || tamanho_individuo == -1) {
		std::cerr << "tamanhos inválidos" << std::endl;
		std::cerr << "Use " << programa << " criar -h para ajuda" << std::endl;
		exit(EXIT_FAILURE);
	}
	acao_criar(tamanho_populacao, tamanho_individuo);
}

static void print_visualizar_usage(char* programa) {
    std::cerr << "Comando Visualizar" << std::endl;
    std::cerr << "Uso: " << programa << " visualizar -f populacao.txt [-t parallel|aptidao]" << std::endl;
}

static void parse_visualizar_args(char* programa, int argc, char* argv[]) {
	if (argc == 1) {
        std::cerr << "Esperava opções" << std::endl;
		std::cerr << "use " << programa << " visualizar -h para ajuda" << std::endl;
		exit(0);
    }
    int c;
    char *nome_arquivo;
    char *tipo_visualizacao = nullptr;
    while((c = getopt(argc, argv, "f:t:h")) != -1){
    	//std::cerr << "opcao: " << optopt << " valor: " << optarg << std::endl;
        switch(c) {
        case 'f':
            nome_arquivo = optarg;
            break;
        case 't':            
            tipo_visualizacao = optarg;
            break;
        case 'h':
            print_visualizar_usage(programa);
            exit(EXIT_SUCCESS);
        case '?':
			std::cerr << "Use " << programa << " visualização -h para ajuda" << std::endl;
            exit(EXIT_FAILURE);
    	}
	}
	acao_visualizar(nome_arquivo, tipo_visualizacao);
}

static void print_evoluir_usage(char* programa) {
    std::cerr << "Comando Evoluir" << std::endl;
    std::cerr << "Uso: " << programa << " evoluir -f populacao.txt -a [sphere|msd|rastrigin]" << std::endl;
}

static void parse_evoluir_args(char* programa, int argc, char* argv[]) {
	if (argc == 1) {
        std::cerr << "Esperava opções" << std::endl;
		std::cerr << "use " << programa << " evoluir -h para ajuda" << std::endl;
		exit(0);
    }
    int c;
    char *nome_arquivo;
    char *aptidao = nullptr;
    while((c = getopt(argc, argv, "f:a:h")) != -1){
    	//std::cerr << "opcao: " << optopt << " valor: " << optarg << std::endl;
        switch(c) {
        case 'f':
            nome_arquivo = optarg;
            break;
        case 'a':            
            aptidao = optarg;
            break;
        case 'h':
            print_evoluir_usage(programa);
            exit(EXIT_SUCCESS);
        case '?':
			std::cerr << "Use " << programa << " evoluir -h para ajuda" << std::endl;
            exit(EXIT_FAILURE);
    	}
	}
	acao_evoluir(nome_arquivo, aptidao);
}

void parse_args(int argc, char* argv[]) {
	char* programa = argv[0];

	if (argc < 2) {
		std::cerr << "Era esperado algum comando" << std::endl;
		std::cerr << "use -h para ajuda" << std::endl;
		return;
	}

	std::string cur_value(argv[1]);
	if (cur_value == "criar") {
		parse_criar_args(programa, argc - 1, argv + 1);
	} else if (cur_value == "visualizar") {
		parse_visualizar_args(programa, argc - 1, argv + 1);
	} else if (cur_value == "evoluir") {
		parse_evoluir_args(programa, argc - 1, argv + 1);
	} else if (cur_value == "-h") {
		print_usage(argv[0]);
		exit(0);
	} else {
		std::cerr << "comando \"" << cur_value << "\" desconhecido" << std::endl;
		std::cerr << "use -h para ajuda" << std::endl;
		exit(1);
	}
}